#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

chart_url="https://dependabot-gitlab.gitlab.io/chart"

log "Package helm chart"
cp README.md LICENSE "$CHART_DIR/"
helm package --dependency-update "$CHART_DIR"

log "Fetch index.yaml from ${chart_url}"
curl -f -o index.yaml "${chart_url}/index.yaml"

log "Update index.yml"
helm repo index . --merge index.yaml --url https://storage.googleapis.com/dependabot-gitlab
mv index.yaml public/
